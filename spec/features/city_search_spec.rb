require 'rails_helper'

feature 'User does a city search' do
  before(:each) do
    stub_request(:get, 'http://api.openweathermap.org/data/2.5/forecast/daily?appid=ba68e2c43979c6eabbd33ff8e32c0611&cnt=16&q=dublin&units=metric').
      to_return(status: 200, body: File.read("spec/fixtures/valid_forecast_api_response.json"), headers: {})
  end

  scenario 'the see the 16 day forecast' do
    visit root_path

    fill_in 'city_name', with: 'dublin'
    click_button 'Go'

    expect(page).to have_content('16 Day Forecast For dublin')
  end
end