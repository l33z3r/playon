require 'rails_helper'

RSpec.describe WeatherService::Api, type: :model do
  subject { described_class.new(city_name: 'dublin') }

  describe '#sixteen_day_forecast' do
    context 'valid city name' do
      before(:each) do
        stub_request(:get, 'http://api.openweathermap.org/data/2.5/forecast/daily?appid=ba68e2c43979c6eabbd33ff8e32c0611&cnt=16&q=dublin&units=metric').
          to_return(status: 200, body: File.read("spec/fixtures/valid_forecast_api_response.json"), headers: {})
      end

      it 'returns a valid forecast' do
        result = subject.sixteen_day_forecast

        expect(result.count).to eq(16)
      end
    end

    context 'invalid city name' do
      before(:each) do
        stub_request(:get, 'http://api.openweathermap.org/data/2.5/forecast/daily?appid=ba68e2c43979c6eabbd33ff8e32c0611&cnt=16&q=dublin&units=metric').
          to_return(status: 404, body: '', headers: {})
      end

      it 'throws an error' do
        expect { subject.sixteen_day_forecast }.to raise_error(RestClient::NotFound)
      end
    end
  end
end