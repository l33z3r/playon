require 'rails_helper'

RSpec.describe Forecast::DaysController, type: :controller do
  describe "GET #index" do
    context 'Valid API Response' do
      before(:each) do
        stub_request(:get, 'http://api.openweathermap.org/data/2.5/forecast/daily?appid=ba68e2c43979c6eabbd33ff8e32c0611&cnt=16&q=dublin&units=metric').
          to_return(status: 200, body: File.read("spec/fixtures/valid_forecast_api_response.json"), headers: {})
      end

      it "returns http success and sets @days" do
        get :index, params: { city_name: 'dublin' }
        expect(response).to have_http_status(:success)
      end
    end

    context 'Invalid API Response' do
      before(:each) do
        stub_request(:get, 'http://api.openweathermap.org/data/2.5/forecast/daily?appid=ba68e2c43979c6eabbd33ff8e32c0611&cnt=16&q=dublin&units=metric').
          to_return(status: 404, body: '', headers: {})
      end

      it "returns http success and sets @days" do
        get :index, params: { city_name: 'dublin' }
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to eq("City dublin not found")
      end
    end
  end
end
