module WeatherService
  class Day
    attr_reader :date, :wind_speed, :humidity, :pressure, :avg_temp,
                :weather, :icon, :morning_temp, :evening_temp, :night_temp

    def initialize(day_json)
      @date = Time.at(day_json['dt'])
      @wind_speed = day_json['speed']
      @humidity = day_json['humidity']
      @pressure = day_json['pressure']
      @avg_temp = day_json['temp']['day']
      @weather = day_json['weather'][0]['main']
      @icon = day_json['weather'][0]['icon']
      @morning_temp = day_json['temp']['morn']
      @evening_temp = day_json['temp']['eve']
      @night_temp = day_json['temp']['night']
    end
  end
end