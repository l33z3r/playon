module WeatherService
  class Api
    def initialize(city_name:)
      @city_name = city_name
    end

    def sixteen_day_forecast
      api_response['list'].map do |day_details|
        Day.new(day_details)
      end
    end

    def specific_day_breakdown(day_number:)
      Day.new(api_response['list'][day_number.to_i - 1])
    end

    private

    def api_response
      api_response = Rails.cache.fetch("forecast_#{@city_name}") do
        params = {
          q: @city_name,
          appid: api_key,
          cnt: 16,
          units: 'metric'
        }

        JSON.parse(RestClient.get url, { params: params })
      end

      api_response
    end

    def api_key
      Rails.configuration.x.openweathermap.api_key
    end

    def url
      Rails.configuration.x.openweathermap.url
    end
  end
end