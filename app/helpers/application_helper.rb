module ApplicationHelper
  def nice_date date
    date.strftime("%A, %-d %b")
  end
end
