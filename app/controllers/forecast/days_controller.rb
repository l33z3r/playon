class Forecast::DaysController < ApplicationController
  before_action :set_city_name

  def index
    handle_api_errors do
      @days = WeatherService::Api.new(city_name: @city_name).sixteen_day_forecast
    end
  end

  def show
    handle_api_errors do
      @day_number = params[:id]
      @day = WeatherService::Api.new(city_name: @city_name).specific_day_breakdown(day_number: @day_number)
    end
  end

  private

  def handle_api_errors
    yield
  rescue RestClient::NotFound => e
    redirect_to root_path, alert: "City #{@city_name} not found"
  rescue SocketError, RestClient::Exception => e
    Rails.logger.error "Error contacting API: #{e.message}\n #{e.backtrace.join("\n ")}"
    redirect_to root_path, alert: 'Error contacting API'
  end

  def set_city_name
    redirect_to root_path, alert: 'Please enter a city' if params[:city_name].blank?

    @city_name = params[:city_name]
  end
end
