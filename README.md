# README

I went for a REST style solution using the index action in days_controller to list out the 16 day forecast and then the show action for a particular day. 
For the show action, the ID is actually the day number which indexes into the 'list' attribute in the API response 

As it's not creating records or manipulating anything in the local DB, I did not use strong parameters. 
I was even considering not using restful routes at all, but I think it makes for a cleaner solution.

I've used caching in redis on production for the API response. 
The main work is done in the WeatherService::Api class. 
I added a services directory for this class.

In the specs I use webmock to mock a successful and unsuccessful request to the API
There are some controller tests and also a simple integration test using capybara.

For styling I have just used some basic bootstrap.

I've set up an EC2 instance on Amazon using elastic beanstalk and I've set up a gitlab server that uses CI/CD. 
The rspec tests are run there and if they pass, then the code automatically deploys there. 

The url for the app running is: http://production.xcmk3cqh4m.eu-west-1.elasticbeanstalk.com/