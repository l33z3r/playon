Rails.application.routes.draw do
  root 'home#index'

  namespace :forecast do
    resources :days, only: [:index, :show]
  end
end
